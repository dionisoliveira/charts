﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using ChartsF.WS.ServicesPrecosDiariosInternacionais;
using ChartsF.WS.ServicesPrecosDiariosRecebidos;
using ChartsF.WS.ServicePrecosDiariosAtacado;
using ChartsF.WS.Model;
using ChartsF.ChartBLL;



namespace ChartsF.WS
{
    public class CustomClient
    {

        public string DownloadStrings(string url)
        {

         
            return string.Empty;

        }

       
        #region Internacional

        public async Task<List<PrecoInternacional>> GetPrecosInternacionais90Dias()
        {
            var requesta = new PrecosDiariosInternacionais();
            List<ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco> listPrecosInternacionaisProdutos = new List<ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco>();
            var soap = new PrecosDiariosInternacionais();
			BaseAccessWS<PrecoInternacional> connectionData = new BaseAccessWS<PrecoInternacional>();
			for (int i = 0; i <= 10; i++)
            {
				var listItem = soap.NaData (DateTime.Now.Date.AddDays (-i), "Ta21w1x").ToList ();
				foreach(ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco item in listItem.Where(p=> string.IsNullOrEmpty(p.Valor.ToString())))
					{

					var precoItem =  connectionData.Connection.Table<ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco>().Where(p => p.Produto == item.Produto && p.Mercado == p.Mercado && p.Unidade == p.Unidade).OrderBy(p=>p.Data).LastOrDefault();
					  
					if (precoItem != null && precoItem.Valor > 0) {
						item.Valor = precoItem.Valor;
					} else {

						item.Valor = 0;
					}
						
					}
				listPrecosInternacionaisProdutos.AddRange(listItem);
            }
            return listPrecosInternacionaisProdutos.Cast<PrecoInternacional>().ToList();
        }
        public List<PrecoInternacional> GetPrecosInternacionaisHoje()
        {
            var soap = new PrecosDiariosInternacionais();
            return soap.NaData(DateTime.Now.Date, "Ta21w1x").ToList().Cast<PrecoInternacional>().ToList();
        }

        #endregion
        #region Produtores
        public async Task<List<PrecoProdutor>> GetPrecosProdutor()
        {
            var soap = new PrecosDiariosRecebidos();
            List<ChartsF.WS.ServicesPrecosDiariosRecebidos.Preco> listPrecosInternacionaisProdutos = new List<ChartsF.WS.ServicesPrecosDiariosRecebidos.Preco>();

			for (int i = 0; i <= 10; i++)
            {


                listPrecosInternacionaisProdutos.AddRange(soap.NaData(DateTime.Now.Date.AddDays(-i), "Ta21w1x").ToList());
            }
            return listPrecosInternacionaisProdutos.Cast<PrecoProdutor>().ToList();
        }
        public List<PrecoProdutor> GetPrecoProdutorHoje(DateTime ultimaData)
        {
            List<ChartsF.WS.ServicesPrecosDiariosRecebidos.Preco> listPrecoProdutos = new List<ChartsF.WS.ServicesPrecosDiariosRecebidos.Preco>();
           
                var soap = new PrecosDiariosRecebidos();
                
                BaseAccessWS<PrecoProdutor> connectionData = new BaseAccessWS<PrecoProdutor>();
                TimeSpan date = DateTime.Now - ultimaData;

                for (int i = 0; i <= date.Days; i++)
                {

                    listPrecoProdutos.AddRange(soap.NaData(ultimaData.AddDays(i), "Ta21w1x").ToList());

					var listItem = soap.NaData (DateTime.Now.Date.AddDays (-i), "Ta21w1x").ToList ();
					foreach(PrecoProdutor item in listItem.Where(p=> string.IsNullOrEmpty(p.Valor.ToString())))
					{

						var precoItem = connectionData.Connection.Table<PrecoProdutor>().Where(p => p.Produto == item.Produto && p.Edr == p.Edr && p.Unidade == p.Unidade).OrderBy(p=>p.Data).LastOrDefault();

						if (precoItem != null && precoItem.Valor > 0) {
							item.Valor = precoItem.Valor;
						} else {

							item.Valor = 0;
						}

					}
					listPrecoProdutos.AddRange(listItem);

                }
                if (date.Days > 0 && listPrecoProdutos.Any())
                {
                    connectionData.DeletaPrecosProdutoByDatas(ultimaData, DateTime.Now);

                }

            return listPrecoProdutos.Cast<PrecoProdutor>().ToList();

        }

        #endregion

        #region Atacado
        public async Task<List<PrecoAtacado>> GetPrecosAtacado90Dias()
        {
            var soap = new PrecosDiariosAtacado();
            List<ChartsF.WS.ServicePrecosDiariosAtacado.Preco> listPrecosInternacionaisProdutos = new List<ChartsF.WS.ServicePrecosDiariosAtacado.Preco>();

            for (int i = 0; i <= 10; i++)
            {
                listPrecosInternacionaisProdutos.AddRange(soap.NaData(DateTime.Now.Date.AddDays(-i), "Ta21w1x").ToList());

            }
            var preco = listPrecosInternacionaisProdutos.Cast<PrecoAtacado>();
            return listPrecosInternacionaisProdutos.Cast<PrecoAtacado>().ToList();
        }
        public List<PrecoAtacado> GetPrecoAtacadorHoje()
        {
            var soap = new PrecosDiariosAtacado();
            List<PrecoAtacado> precoAtacado = new List<PrecoAtacado>();
            precoAtacado.AddRange(soap.NaData(DateTime.Now.Date, "Ta21w1x").ToList().Cast<PrecoAtacado>().ToList());
            precoAtacado.AddRange(soap.NaData(DateTime.Now.Date.AddDays(-1), "Ta21w1x").ToList().Cast<PrecoAtacado>().ToList());
            return precoAtacado;
        }

        #endregion

    }
}
