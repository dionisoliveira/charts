﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.WS.Model
{
    [Table("PrecoAtacado")]
    public class PrecoAtacado : IModelBase
    {
		[AutoIncrement, PrimaryKey]
		public int id { get; set;}
        [Column("Origem")]
        /// <remarks/>
        public string Origem { get; set; }
        [Column("Produto")]
        /// <remarks/>
        public string Produto { get; set; }
        [Column("Descricao")]
        /// <remarks/>
        public string Descricao { get; set; }
        [Column("Unidade")]
        /// <remarks/>
        public string Unidade { get; set; }
        [Column("Data")]
        /// <remarks/>
        public System.DateTime Data { get; set; }
        [Column("Minimo")]
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<decimal> Minimo { get; set; }
        [Column("MaisComum")]
        // [Column("id"), PrimaryKey]/ <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<decimal> MaisComum { get; set; }
        [Column("Maximo")]
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<decimal> Maximo { get; set; }
    }

}
