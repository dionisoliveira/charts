﻿


using System;
using SQLite;
using System.Collections.Generic;
using ChartsF.WS;
using ChartsF.WS.Model;


namespace ChartsF.ChartBLL
{
    public class BaseAccessWS<T>
    {


		private readonly SQLiteAsyncConnection _connection;

		public SQLiteAsyncConnection Connection
        {
            get { return _connection; }

        }




        public BaseAccessWS()
        {




            _connection = new SQLite.SQLiteConnection(HelpersUtils.PatchDatabase);
        }


        public void CreateDataBase(string path)
        {
            try
            {

                HelpersUtils.PatchDatabase = path;
                _connection = new SQLite.SQLiteConnection(path);
                _connection.CreateTable<PrecoInternacional>();
                _connection.CreateTable<PrecoProdutor>();
                _connection.CreateTable<PrecoAtacado>();
                _connection.CreateTable<Cidade>();



            }
            catch (SQLiteException ex)
            {
                throw ex;
            }


        }

        public virtual T Insert(T model)
        {
            Connection.Insert(model);
            return model;
        }





        public virtual T Update(T model)
        {
            Connection.Update(model);
            return model;
        }

        public virtual T Delete(T model)
        {
            Connection.Delete(model);
            return model;
        }

        public virtual void DeleteAll()
        {
            Connection.DeleteAll<T>();

        }

        public virtual IList<T> InsertAll(IList<T> model)
        {
            Connection.InsertAll(model);
            return model;

        }

        public virtual void DeletaPrecosProdutoByDatas(DateTime dataInit, DateTime DataFim)
        {
            var edrObject = new object[2];
            edrObject[0] = dataInit;
            edrObject[1] = DataFim;

            Connection.Execute("DELETE FROM PrecoProdutor WHERE data between ? and ?", edrObject);


        }






    }
}

