﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.WS.Model
{

    [Table("Cidade")]
    public class Cidade : IModelBase
    {

		[AutoIncrement, PrimaryKey]
		public int id { get; set;}
        [Column("Nome")]
        public string Nome { get; set; }
    }
}
