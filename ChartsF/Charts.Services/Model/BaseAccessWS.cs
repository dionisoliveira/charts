﻿


using ChartsF.WS.Model;

using SQLite;
using System.Linq;
using System.Collections.Generic;
using System;
using ChartsF.WS;
using System.Linq.Expressions;

namespace ChartsF.ChartBLL
{
    public class BaseAccessWS<T>
    {


        private SQLiteConnection _connection;

        public SQLiteConnection Connection
        {
            get { return _connection; }

        }




        public BaseAccessWS()
        {




            _connection = new SQLite.SQLiteConnection(HelpersUtils.PatchDatabase);
        }


        public void CreateDataBase(string path)
        {
            try
            {

                HelpersUtils.PatchDatabase = path;
                _connection = new SQLite.SQLiteConnection(path);
                _connection.CreateTable<PrecoInternacional>();
                _connection.CreateTable<PrecoProdutor>();
                _connection.CreateTable<PrecoAtacado>();
                _connection.CreateTable<Cidade>();



            }
            catch (SQLiteException ex)
            {
                throw ex;
            }


        }

        public virtual T Insert(T model)
        {
            Connection.Insert(model);
            return model;
        }





        public virtual T Update(T model)
        {
            Connection.Update(model);
            return model;
        }

        public virtual T Delete(T model)
        {
            Connection.Delete(model);
            return model;
        }

        public virtual void DeleteAll()
        {
            Connection.DeleteAll<T>();

        }

        public virtual IList<T> InsertAll(IList<T> model)
        {
            Connection.InsertAll(model);
            return model;

        }

        public virtual void DeletaPrecosProdutoByDatas(DateTime dataInit, DateTime DataFim)
        {
            var edrObject = new object[2];
            edrObject[0] = dataInit;
            edrObject[1] = DataFim;

            Connection.Execute("DELETE FROM PrecoProdutor WHERE data between ? and ?", edrObject);


        }






    }
}

