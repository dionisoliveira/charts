﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Charts.Services;

namespace ChartsF.WS.Model
{

     [Table("PrecoInternacional")]
    public class PrecoInternacional : IModelBase
    {

		[AutoIncrement, PrimaryKey]
		public int id { get; set;}
            [Column("Produto")]
         public string Produto { get; set; }

            [Column("Mercado")]
            public string Mercado { get; set; }

            [Column("Entrada")]
            public string Entrada { get; set; }

            [Column("Unidade")]
            public string Unidade { get; set; }

            [Column("Data")]
            public System.DateTime Data { get; set; }

            [Column("Valor")]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
            public System.Nullable<decimal> Valor { get; set; }

            [Column("Obs")]
            public string Obs { get; set; }
    }
}
