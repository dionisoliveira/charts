﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Charts.Services;

namespace ChartsF.WS.Model
{
    [Table("PrecoProdutor")]

    public class PrecoProdutor : IModelBase
    {
		[AutoIncrement, PrimaryKey]
		public int id { get; set;}
        [Column("Produto")]
        public string Produto { get; set; }

        [Column("Unidade")]
        public string Unidade { get; set; }

        [Column("Edr")]
        public string Edr { get; set; }

        [Column("Data")]
        public System.DateTime Data { get; set; }

        [Column("Valor")]
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<decimal> Valor { get; set; }

        [Column("Obs")]
        public string Obs { get; set; }

        public string Variacao { get; set; }
    }
}
