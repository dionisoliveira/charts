﻿using ChartsF.WS.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.WS
{
    public static class HelpersUtils
    {
        public static string PatchDatabase;

        public static string GetStringResource(Assembly assembly, string resourcePath)
        {
            Stream stream = assembly.GetManifestResourceStream(resourcePath);
            string returnString = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                returnString = reader.ReadToEnd();
            }
            return returnString;
        }

        public static long ToJsonTicks(DateTime value)
        {
            return (value.ToUniversalTime().Ticks - ((new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks)) / 10000;
        }

        public static List<string> GetItemGraficoPrecoAtacado(List<PrecoAtacado> listStock = null)
        {
            var valueJson = new List<string>();

            if (!listStock.Any())
            {

                foreach (PrecoAtacado stock in listStock.OrderBy(p => p.Data).ToList())
                {

                    valueJson.Add("[" + ToJsonTicks(stock.Data) + "," + stock.MaisComum.ToString().Replace(",", ".") + "]");
                }
            }


            return valueJson;
        }

        public static List<string> GetItemGraficoPrecoProdutor(List<PrecoProdutor> listStock = null)
        {
            var valueJson = new List<string>();

            if (listStock.Any())
            {

                foreach (PrecoProdutor stock in listStock.Where(p=> p.Valor > 0 ).OrderBy(p => p.Data ).ToList())
                {
                    if (stock.Valor != 0)
                    {
                        valueJson.Add("[" + ToJsonTicks(stock.Data) + "," + stock.Valor.ToString().Replace(",", ".") + "]");
                    }
                    else
                    {
                        var stokList = listStock.Where(p => p.Valor > 0).OrderBy(p => p.Data).FirstOrDefault();
                        if (stokList != null)
                        {
                            valueJson.Add("[" + ToJsonTicks(stock.Data) + "," + stokList.Valor.ToString().Replace(",", ".") + "]");
                        }
                        else
                        {
                            valueJson.Add("[" + ToJsonTicks(stock.Data) + "," + 0.ToString() + "]");
                        }
                    }
                }
            }


            return valueJson;
        }

        public static List<string> GetItemGraficoPrecoInternacional(List<PrecoInternacional> listStock = null)
        {
            var valueJson = new List<string>();

            if (!listStock.Any())
            {

                foreach (PrecoInternacional stock in listStock.OrderBy(p => p.Data).ToList())
                {
                    valueJson.Add("[" + ToJsonTicks(stock.Data) + "," + stock.Valor.ToString().Replace(",", ".") + "]");
                }
            }


            return valueJson;
        }
    }


}
