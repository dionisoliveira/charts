﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

using ChartsF.WS.Model;
using ChartsF.ChartBLL;
using iea.sp.gov.br;
using System.Threading;



namespace ChartsF.WS
{
    public class CustomClient
    {

        public string DownloadStrings(string url)
        {

         
            return string.Empty;

        }

       
        #region Internacional

//        public async Task<List<PrecoInternacional>> GetPrecosInternacionais90Dias()
//        {
//            var requesta = new PrecosDiariosInternacionais();
//            List<ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco> listPrecosInternacionaisProdutos = new List<ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco>();
//            var soap = new PrecosDiariosInternacionais();
//			BaseAccessWS<PrecoInternacional> connectionData = new BaseAccessWS<PrecoInternacional>();
//			for (int i = 0; i <= 10; i++)
//            {
//				var listItem = soap.NaData (DateTime.Now.Date.AddDays (-i), "Ta21w1x").ToList ();
//				foreach(ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco item in listItem.Where(p=> string.IsNullOrEmpty(p.Valor.ToString())))
//					{
//
//					var precoItem =  connectionData.Connection.Table<ChartsF.WS.ServicesPrecosDiariosInternacionais.Preco>().Where(p => p.Produto == item.Produto && p.Mercado == p.Mercado && p.Unidade == p.Unidade).OrderBy(p=>p.Data).LastOrDefault();
//					  
//					if (precoItem != null && precoItem.Valor > 0) {
//						item.Valor = precoItem.Valor;
//					} else {
//
//						item.Valor = 0;
//					}
//						
//					}
//				listPrecosInternacionaisProdutos.AddRange(listItem);
//            }
//            return listPrecosInternacionaisProdutos.Cast<PrecoInternacional>().ToList();
//        }
//        public List<PrecoInternacional> GetPrecosInternacionaisHoje()
//        {
//            var soap = new PrecosDiariosInternacionais();
//            return soap.NaData(DateTime.Now.Date, "Ta21w1x").ToList().Cast<PrecoInternacional>().ToList();
//        }
//
        #endregion
        #region Produtores
		public async Task<object> GetPrecosProdutor(DateTime date)
        {
			
			EndpointAddress endPoint = new EndpointAddress("http://ciagri.iea.sp.gov.br/PrecosDiarios/WebServices/PrecosDiariosRecebidos.asmx");
			PrecosDiariosRecebidosSoap _client;


			_client = new PrecosDiariosRecebidosSoapClient(new
				BasicHttpBinding(BasicHttpSecurityMode.None), endPoint);
		
		
			var request = new NaDataRequest ();
			request.Body = new NaDataRequestBody ();
			request.Body.data = date;
			request.Body.token = "Ta21w1x";
		
			var res =  Task<object>.Factory.FromAsync(_client.BeginNaData, _client.EndNaData, request, null);
		
			object taskResult =await res;
		
			return taskResult;

         
        }

        public List<PrecoProdutor> GetPrecoProdutorHoje(DateTime ultimaData)
        {
			int count = 0;
			EndpointAddress endPoint = new EndpointAddress("http://192.168.216.1:1234/CalculatorService.svc");

			BasicHttpBinding binding = new BasicHttpBinding
			{
				Name = "basicHttpBinding",
				MaxBufferSize = 2147483647,
				MaxReceivedMessageSize = 2147483647
			};
			var resetEvent = new ManualResetEvent(false);
			PrecosDiariosRecebidosSoapClient client = new PrecosDiariosRecebidosSoapClient(binding, endPoint);

                
                BaseAccessWS<PrecoProdutor> connectionData = new BaseAccessWS<PrecoProdutor>();
                TimeSpan date = DateTime.Now - ultimaData;

			List<PrecoProdutor> listPrecosInternacionaisProdutos = new List<PrecoProdutor>();
			client.NaDataCompleted += (sender, e) => {

				listPrecosInternacionaisProdutos.AddRange(e.Result.ToList().Cast<PrecoProdutor>());
				count+=1;
				if(count == date.Days)
					resetEvent.Set();
			};


			for (int i = 0; i <= date.Days; i++) {
				client.NaDataAsync (DateTime.Now.Date.AddDays (-i), "Ta21w1x");
                   

			
			}
					

                

			resetEvent.WaitOne ();
			if (date.Days > 0 && listPrecosInternacionaisProdutos.Any ()) {
				connectionData.DeletaPrecosProdutoByDatas (ultimaData, DateTime.Now);

			}
			connectionData.InsertAll(listPrecosInternacionaisProdutos);
			return listPrecosInternacionaisProdutos.Cast<PrecoProdutor>().ToList();

        }

        #endregion

//        #region Atacado
//        public async Task<List<PrecoAtacado>> GetPrecosAtacado90Dias()
//        {
//            var soap = new PrecosDiariosAtacado();
//            List<ChartsF.WS.ServicePrecosDiariosAtacado.Preco> listPrecosInternacionaisProdutos = new List<ChartsF.WS.ServicePrecosDiariosAtacado.Preco>();
//
//            for (int i = 0; i <= 10; i++)
//            {
//                listPrecosInternacionaisProdutos.AddRange(soap.NaData(DateTime.Now.Date.AddDays(-i), "Ta21w1x").ToList());
//
//            }
//            var preco = listPrecosInternacionaisProdutos.Cast<PrecoAtacado>();
//            return listPrecosInternacionaisProdutos.Cast<PrecoAtacado>().ToList();
//        }
//        public List<PrecoAtacado> GetPrecoAtacadorHoje()
//        {
//            var soap = new PrecosDiariosAtacado();
//            List<PrecoAtacado> precoAtacado = new List<PrecoAtacado>();
//            precoAtacado.AddRange(soap.NaData(DateTime.Now.Date, "Ta21w1x").ToList().Cast<PrecoAtacado>().ToList());
//            precoAtacado.AddRange(soap.NaData(DateTime.Now.Date.AddDays(-1), "Ta21w1x").ToList().Cast<PrecoAtacado>().ToList());
//            return precoAtacado;
//        }
//
//        #endregion

    }
}
