﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Xamarin.Forms;
using ChartsF;
using Windows.Storage;
using System.IO;

namespace ChartsF.WinPhone
{
    public partial class MainPage : global::Xamarin.Forms.Platform.WinPhone.FormsApplicationPage
    {
        public MainPage()
        {
          
            InitializeComponent();
            global::Xamarin.Forms.Forms.Init();
            SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;
            var storageFile = System.IO.IsolatedStorage.IsolatedStorageFile.GetUserStoreForApplication();
            
          
            var sqliteFilename = "AGRO_SP.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);

           
            
            if (!Directory.Exists(ApplicationData.Current.LocalFolder.Path))
            {
                Directory.CreateDirectory(ApplicationData.Current.LocalFolder.Path);
                if (!File.Exists(path))
                    File.Create(path);
            }

            var plat = new SQLite.Net.Platform.WindowsPhone8.SQLitePlatformWP8();
            var conn = new SQLite.Net.SQLiteConnection(plat, path);

       
            LoadApplication(new ChartsF.App(conn)); // new in 1.3
           
            
          
          
        }

        

    }
}
