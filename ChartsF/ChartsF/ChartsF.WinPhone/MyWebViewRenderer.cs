﻿using Microsoft.Phone.Controls;
using System;
using ChartsF;
using ChartsF.WinPhone;
using Microsoft.Phone.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;


[assembly: ExportRenderer(typeof(MyWebView), typeof(MyWebViewRenderer))]

namespace ChartsF.WinPhone

{

    public  class MyWebViewRenderer : WebViewRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement == null)
            {
                var webView = (WebBrowser)Control;
                webView.IsScriptEnabled = true;
                webView.Height = 600;
              
            }
        }
       
    }
}
