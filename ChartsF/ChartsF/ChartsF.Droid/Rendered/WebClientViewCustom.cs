using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Xamarin.Forms;

namespace ChartsF.Droid.Rendered
{
    public class WebClientViewCustom : WebViewClient 
    {


        public override bool ShouldOverrideUrlLoading(Android.Webkit.WebView view, string url)
        {

        
             var uri = Android.Net.Uri.Parse (url);
             var intent = new Intent(Intent.ActionView, uri);
            Forms.Context.StartActivity(intent);
            return true;
         
        }
    }
}