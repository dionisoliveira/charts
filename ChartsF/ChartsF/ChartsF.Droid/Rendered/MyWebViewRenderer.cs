﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Java.Interop;
using ChartsF;
using ChartsF.Droid.Rendered;
using ChartsF.Droid;
using SIGGA.GESTAOPARTICIPATIVA.CustomUIComponents.View;



[assembly: ExportRenderer(typeof(Android.Webkit.WebView), typeof(MyWebViewRenderer))]
namespace ChartsF.Droid.Rendered
{

    public class MyWebViewRenderer : WebViewRenderer
    {
        private global::Android.Webkit.WebView _nativeWebView;
        private CustomWebView _formsView;

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
                //Configura webview nativa
                _nativeWebView = (global::Android.Webkit.WebView)Control;
                _nativeWebView.Settings.JavaScriptEnabled = true;
                _nativeWebView.Settings.SetPluginState(global::Android.Webkit.WebSettings.PluginState.On);
                _nativeWebView.Settings.DomStorageEnabled = true;
      

                /* //Configura webview forms
                 _formsView = (CustomWebView)e.NewElement;
                 _formsView.CanGoBackUpdater = CanGoBackUpdater;
                 _formsView.GoBack = GoBackMethod;*/

                //Define se deve ter loading
                _nativeWebView.SetWebViewClient(new WebClientViewCustom());﻿
                _nativeWebView.SetWebChromeClient(new global::Android.Webkit.WebChromeClient());
                this.Click += MyWebViewRenderer_Click;

            }
        }

        public override void SetOnClickListener(Android.Views.View.IOnClickListener l)
        {
            base.SetOnClickListener(l);
        }
        public override bool ShouldDelayChildPressedState()
        {
            return base.ShouldDelayChildPressedState();
        }
        void MyWebViewRenderer_Click(object sender, EventArgs e)
        {
            var uri = Android.Net.Uri.Parse("http://www.xamarin.com");
            var intent = new Intent(Intent.ActionView, uri);
            Forms.Context.StartActivity(intent); 
        }

        void _nativeWebView_KeyPress(object sender, Android.Views.View.KeyEventArgs e)
        {
            var uri = Android.Net.Uri.Parse("http://www.xamarin.com");
            var intent = new Intent(Intent.ActionView, uri);
            Forms.Context.StartActivity(intent); 
        }

       

        private void GoBackMethod()
        {
            _nativeWebView.GoBack();
        }

        private void CanGoBackUpdater()
        {
            _formsView.CanGoBack = _nativeWebView.CanGoBack();
        }

        #region Client Implementation

        public class CustomWebChromeClient : global::Android.Webkit.WebChromeClient
        {
            private ProgressDialog _progress;

            public CustomWebChromeClient()
                : base()
            {
                _progress = new ProgressDialog(Forms.Context);
                _progress.SetTitle("");
                _progress.SetMessage("");
                _progress.SetCancelable(true);
                _progress.SetProgressStyle(ProgressDialogStyle.Horizontal);
                _progress.Progress = 0;
                _progress.Max = 100;
                _progress.Show();
            }

            public override void OnProgressChanged(global::Android.Webkit.WebView view, int newProgress)
            {
                base.OnProgressChanged(view, newProgress);
                _progress.Progress = newProgress;
                if (newProgress == 100)
                {
                    _progress.Dismiss();
                }
            }
        }

        #endregion
    }

}