﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using System.IO;

namespace ChartsF.Droid
{
    [Activity(Label = "Gráficos", Icon = "@drawable/ic_stock", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity :FormsApplicationActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            string storage = string.Empty;
            if (Android.OS.Environment.ExternalStorageState.Equals(Android.OS.Environment.MediaMounted))
            {
                var file = Android.OS.Environment.ExternalStorageDirectory;
                storage = System.IO.Path.Combine(System.IO.Path.Combine(file.AbsolutePath), "AGRO_SP");
            }
            string dbPath = System.IO.Path.Combine(storage, "AGRO_SP.db");
            if (!Directory.Exists(storage))
            {
                Directory.CreateDirectory(storage);
                if (!File.Exists(dbPath))
                    File.Create(dbPath);
            }
             
            
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App(dbPath));

        }
    }
}

