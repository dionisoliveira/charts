package chartsf.droid.rendered;


public class CustomJavascriptInterface
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("ChartsF.Droid.Rendered.CustomJavascriptInterface, ChartsF.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", CustomJavascriptInterface.class, __md_methods);
	}


	public CustomJavascriptInterface () throws java.lang.Throwable
	{
		super ();
		if (getClass () == CustomJavascriptInterface.class)
			mono.android.TypeManager.Activate ("ChartsF.Droid.Rendered.CustomJavascriptInterface, ChartsF.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
