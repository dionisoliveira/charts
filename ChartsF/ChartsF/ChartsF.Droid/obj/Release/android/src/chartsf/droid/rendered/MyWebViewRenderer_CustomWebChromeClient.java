package chartsf.droid.rendered;


public class MyWebViewRenderer_CustomWebChromeClient
	extends android.webkit.WebChromeClient
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onProgressChanged:(Landroid/webkit/WebView;I)V:GetOnProgressChanged_Landroid_webkit_WebView_IHandler\n" +
			"";
		mono.android.Runtime.register ("ChartsF.Droid.Rendered.MyWebViewRenderer/CustomWebChromeClient, ChartsF.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MyWebViewRenderer_CustomWebChromeClient.class, __md_methods);
	}


	public MyWebViewRenderer_CustomWebChromeClient () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyWebViewRenderer_CustomWebChromeClient.class)
			mono.android.TypeManager.Activate ("ChartsF.Droid.Rendered.MyWebViewRenderer/CustomWebChromeClient, ChartsF.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onProgressChanged (android.webkit.WebView p0, int p1)
	{
		n_onProgressChanged (p0, p1);
	}

	private native void n_onProgressChanged (android.webkit.WebView p0, int p1);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
