﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using ChartsF;
using ChartsF.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(WebView), typeof(MyWebViewRenderer))]
namespace ChartsF.iOS
{
	


	public class MyWebViewRenderer :WebViewRenderer
	{
		private string height="____height____";
		private string width="____width____";

		public override void LoadHtmlString (string s, Foundation.NSUrl baseUrl)
		{
			if (baseUrl == null)
				baseUrl = new NSUrl (NSBundle.MainBundle.BundlePath, true);
			
			var heigths = "height:" +base.Frame.Height.ToString () + "px;";
			var widths = "width:"+ base.Frame.Width.ToString () + "px";
			s.Replace(height,heigths );
			s.Replace(width,widths);

			base.LoadHtmlString (s, baseUrl);
	

		


		}

	
	}
}

