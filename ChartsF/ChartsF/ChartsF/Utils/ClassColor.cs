﻿using System;
using Xamarin.Forms;

namespace ChartsF
{
	public class ClassColor
	{
		

		public static Color SeparatorColor =  Color.FromHex ("6fc833");//Sinza
		public static Color BackgroundColor = Color.FromHex("6fc833");//Branco
		public static Color NegativeValueColor =   Color.FromHex("ff0000");//Vermelho
		public static Color PositiveValueColor =   Color.FromHex("2fff00");//Verde
		public static Color MasterPageColor =   Color.FromHex("2fff00");//Verde
		public static Color BackGroudListViewColor =   Color.White;


	}
}

