﻿using ChartsF.DTO;
using ChartsF.WS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.ChartBLL
{
    public class PrecoProdutorBLL
    {



        public static IList<PrecoProdutor> GetPrecosByEdr(string edr)
        {
            var edrObject = new object[1];
            edrObject[0] = edr;
            string select = @"SELECT * FROM PrecoProdutor where Edr = ? ORDER BY Data DESC";

			return  FactoryDataAccess.CreateAccess<PrecoProdutor> ().Query (select, edrObject).Result;
        }


        public  static IList<PrecoProdutor> GetPrecosByProduto_Unidade(string produto,string unidade)
        {
            var edrObject = new object[2];
            edrObject[0] = produto;
            edrObject[1] = unidade;
            string select = @"SELECT * FROM PrecoProdutor where Produto = ? and Unidade = ? ORDER BY Data DESC";

			return  FactoryDataAccess.CreateAccess<PrecoProdutor> ().Query (select, edrObject).Result ;
        }


		public  static IList<PrecoProdutor> GetPrecosByEdr_Produto_Data(string edr,string produto,DateTime data)
        {
            var edrObject = new object[3];
            edrObject[0] = edr;
            edrObject[1] = produto;
            edrObject[2] = data;
            string select = @"SELECT * FROM PrecoProdutor where Edr = ? and Produto = ? and Data = ? ORDER BY Data DESC";

			return FactoryDataAccess.CreateAccess<PrecoProdutor>().Query(select, edrObject).Result;
        }

		public  static IList<PrecoProdutor> GetPrecosByProduto_Edr( string produto,string edr)
        {
            var edrObject = new object[2];
            edrObject[0] = produto;
            edrObject[1] = edr;

            string select = @"SELECT * FROM PrecoProdutor where Produto = ? and Edr = ? ORDER BY Data";

			return  FactoryDataAccess.CreateAccess<PrecoProdutor>().Query(select, edrObject).Result;
        }

		public  static IList<PrecoProdutor> GetPrecosByProduto_Edr_Unidade(string produto, string edr, string unidade)
        {
            var edrObject = new object[3];
            edrObject[0] = produto;
            edrObject[1] = edr;
            edrObject[2] = unidade;

            string select = @"SELECT * FROM PrecoProdutor where Produto = ? and Edr = ? and Unidade = ? ORDER BY Data";

			return  FactoryDataAccess.CreateAccess<PrecoProdutor>().Query(select, edrObject).Result;
        }


        public static IList<ProdutoDTO> GetProdutos()
        {

            string select = @"SELECT (Produto  ||' - '||  Unidade) as ProdutoUnidade,Produto,Unidade,Edr FROM PrecoProdutor  group by Produto,Unidade order by Produto";

			return FactoryDataAccess.CreateAccess<ProdutoDTO>().Query(select, new object[] { }).Result;
        }

        public  static DateTime GetUltimaData()
        {

            string select = @"SELECT MAX(Data) as Data FROM PrecoProdutor";
			return  FactoryDataAccess.CreateAccess<ProdutoDTO>().ExecuteScalar(select, string.Empty).Result.Data;

        }


    }
}
