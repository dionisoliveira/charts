﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.ChartBLL
{
    public interface IDataAcess<T>
    
    {

          T Insert(T model);

         

       

          T Update(T model);


          T Delete(T model);


          void DeleteAll();

          IList<T> InsertAll(IList<T> model);
    }
}
