﻿


using ChartsF.WS.Model;


using System.Linq;
using System.Collections.Generic;
using System;
using ChartsF.WS;

using Charts.Services;
using SQLite;
using System.Threading.Tasks;

namespace ChartsF.ChartBLL
{
	public class BaseAccess<T> : IDataAcess<T>
        where T : IModelBase, new()
	{


		private static SQLiteAsyncConnection _connection;

		public static SQLiteAsyncConnection Connection {
			get { return _connection; }

		}




		public BaseAccess ()
		{


			_connection = new SQLiteAsyncConnection (HelpersUtils.PatchDatabase);
		}


		public static void CreateDataBase (string path)
		{
			try {
				HelpersUtils.PatchDatabase = path;
				_connection = new SQLite.SQLiteAsyncConnection (path);

				_connection.CreateTableAsync<Config> ();
				_connection.CreateTableAsync<PrecoInternacional> ();
				_connection.CreateTableAsync<PrecoProdutor> ();
				_connection.CreateTableAsync<PrecoAtacado> ();
				_connection.CreateTableAsync<Cidade> ();
			



			} catch (SQLiteException ex) {
				throw ex;
			}


		}

		public virtual T Insert (T model)
		{
			Connection.InsertAsync (model);
			return model;
		}

		public async  Task<T> ExecuteScalar (string sql, string key)
		{
			return await Connection.ExecuteScalarAsync<T> (sql, key);
		}

		public int  ExecuteAsync (string sql, params object[] key)
		{
			return Connection.ExecuteAsync(sql, key).Result;
		}

		public virtual List<T> SelectAll ()
		{
			try {

				return Connection.Table<T> ().ToListAsync().Result;
			} catch {
				return null;
			}
		}

		public virtual T Update (T model)
		{
			Connection.UpdateAsync (model);
			return model;
		}

		public virtual T Delete (T model)
		{
			Connection.DeleteAsync (model);
			return model;
		}

		public virtual void DeleteAll ()
		{
			Connection.DropTableAsync<T> ();

		}

		public virtual IList<T> InsertAll (IList<T> model)
		{
			Connection.InsertAllAsync (model);
			return model;

		}

		public Task<List<T>> Query (string query, params object[] args)
		{
			return Connection.QueryAsync<T> (query, args);


		}





	}
}

