﻿using ChartsF.WS;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Charts.Services;

namespace ChartsF.ChartBLL
{
    
	public static class FactoryDataAccess
	{

		public static BaseAccess<T> CreateAccess<T> () where T : IModelBase, new()
		{
			return new BaseAccess<T> ();
		}
		  
	}
    
}
