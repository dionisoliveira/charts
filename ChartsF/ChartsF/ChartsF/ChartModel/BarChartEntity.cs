﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.ChartModel
{
    public class BarChartEntity
    {

        public int ID
        {
            get;
            set;
        }
        public DateTime? Time
        {
            get;
            set;
        }
        public double? Price
        {
            get;
            set;
        }
    }
}
