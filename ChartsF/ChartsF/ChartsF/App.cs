﻿using ChartsF.ChartBLL;
using ChartsF.WS;
using ChartsF.WS.Model;
using Xamarin.Forms;

namespace ChartsF
{
    public class App : Application
    {
       
        public static MasterDetailPage MasterDetailPage;
        public App(string path)
        {

            HelpersUtils.PatchDatabase = path;
            BaseAccess<PrecoAtacado>.CreateDataBase(path);
            MainPage = new RootPage();
        }
      
        public Page GetFirstPage()
        {

            return new RootPage();
        }
   

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
     

    }
}
