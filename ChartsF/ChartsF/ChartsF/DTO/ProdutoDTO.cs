﻿using ChartsF.WS.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.DTO
{
    public class ProdutoDTO : PrecoProdutor
    {

        public string ProdutoUnidade { get; set; }
    }
}
