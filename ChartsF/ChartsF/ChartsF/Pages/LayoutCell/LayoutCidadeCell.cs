﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages.LayoutCell
{
    public class LayoutCidadeCell : ViewCell
    {

        public LayoutCidadeCell()
        {
         
            var nameLayout = CreateNameLayout();

            var viewLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children = {  nameLayout }
            };
            View = viewLayout;
        }


        static StackLayout CreateNameLayout()
        {

            var nomeCidade = new Label
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
			    FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
            };
            nomeCidade.SetBinding(Label.TextProperty, "Nome");

           

            var nameLayout = new StackLayout()
            {
                Spacing = 20, // <- Very important!!
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(8)


            };
            nameLayout.Children.Add(nomeCidade);
          
            return nameLayout;
        }

    }
}
