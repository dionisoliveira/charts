﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{




    public class LayoutCotacaoCell : ViewCell
    {

		public static bool VisibilityLabel = true;

        public LayoutCotacaoCell()
        {



            var nameLayout = CreateNameLayout();

          
			View = nameLayout;
        }
	
        static StackLayout CreateNameLayout()
        {

            var nameLabel = new Label
            {
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
			
				HorizontalOptions = LayoutOptions.StartAndExpand  ,
				LineBreakMode = LineBreakMode.NoWrap
            };
            nameLabel.SetBinding(Label.TextProperty, "ProdutoDescricao");

			var Variacao = new Label
			{		
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				XAlign = TextAlignment.End,
				WidthRequest = 120,
				IsVisible =VisibilityLabel,
				HorizontalOptions = LayoutOptions.End     // AndExpand

			};
			Variacao.SetBinding(Label.TextProperty, "Variacao");
			Variacao.PropertyChanged += Variacao_PropertyChanged;


            var Valor = new Label
            {
				
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				XAlign = TextAlignment.End,
				WidthRequest = 120,
				IsVisible =!VisibilityLabel,
				HorizontalOptions = LayoutOptions.End ,    // AndExpand
            };

			Valor.SetBinding(Label.TextProperty, "Valor");
      
            var nameLayout = new StackLayout()
            {

		
				HorizontalOptions = LayoutOptions.FillAndExpand,     // AndExpand
                Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(10,0),
				Children = {nameLabel}
            };

			var layoutFinal = new StackLayout()
			{

				Spacing = 1,
				HorizontalOptions = LayoutOptions.FillAndExpand,     // AndExpand
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness (10, 5, 10, 5),
				Children = {nameLayout, Variacao,Valor}
			};

		

         
			return layoutFinal;
        }
	
        static void Variacao_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
			

			if (((Label)(sender)).Text.Contains ("-")) {
                   
				((Label)(sender)).TextColor = ClassColor.NegativeValueColor;


			} else if (((Label)(sender)).Text.Contains ("+")) {
                   

				((Label)(sender)).TextColor = ClassColor.PositiveValueColor;
                       
			} 
        }
           


        

    }
}
