﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{



    public class LayoutCotacaoCidadeCell : ViewCell
    {


		public static bool VisibilityLabel = true;
        public LayoutCotacaoCidadeCell()
        {

            var nameLayout = CreateNameLayout();

            var viewLayout = new StackLayout()
            {
                Orientation = StackOrientation.Horizontal,
                Children = { nameLayout }
            };
            View = viewLayout;
        }

        static StackLayout CreateNameLayout()
        {

            var nameLabel = new Label
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,


				LineBreakMode = LineBreakMode.NoWrap,
                TextColor = Color.Black,
            };
            nameLabel.SetBinding(Label.TextProperty, "Erd");

            var Valor = new Label
            {
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				XAlign = TextAlignment.End,
				WidthRequest = 120,
				IsVisible =!VisibilityLabel,
				HorizontalOptions = LayoutOptions.End ,    // AndExpand

            };


            Valor.SetBinding(Label.TextProperty, "Valor");
            var Variacao = new Label
            {
				FontFamily = "HelveticaNeue-Medium",
				FontSize = 18,
				XAlign = TextAlignment.End,
				WidthRequest = 120,
				IsVisible =VisibilityLabel,
				HorizontalOptions = LayoutOptions.End     // AndExpand
					
            };
            Variacao.PropertyChanged += Variacao_PropertyChanged;
            Variacao.SetBinding(Label.TextProperty, "Variacao");

            var nameLayout = new StackLayout()
            {
                Spacing = 20, // <- Very important!!
                HorizontalOptions = LayoutOptions.FillAndExpand,
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(8)


            };
            nameLayout.Children.Add(nameLabel);
            nameLayout.Children.Add(Variacao);
            nameLayout.Children.Add(Valor);
            return nameLayout;
        }

        public static void Variacao_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

			if (((Label)(sender)).Text.Contains ("-")) {


				((Label)(sender)).TextColor = ClassColor.NegativeValueColor;

			} else if (((Label)(sender)).Text.Contains ("+")) {

			
				((Label)(sender)).TextColor = ClassColor.PositiveValueColor;

			} 


        }

    }
}
