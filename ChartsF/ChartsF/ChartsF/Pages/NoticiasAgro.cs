﻿using ChartsF.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ChartsF.WS.Model;
using ChartsF.ChartBLL;
using ChartsF.ChartModel;
using System.Reflection;
using ChartsF.Pages.LayoutCell;
using System.Globalization;


namespace ChartsF.Pages
{
    public class NoticiasAgro : TabbedPage
    {
        public static string htmlPagePath = "ChartsF.WebContent.HTML.notiticas_agro.html";

        public static string  GetHtml()
        {
            var assembly = typeof(NoticiasAgroPage).GetTypeInfo().Assembly;

            //Get html text string
            string htmlTextString = HelpersUtils.GetStringResource(assembly, htmlPagePath);

            var htmlSource = new HtmlWebViewSource();
            var browser = new WebView
            {
               
                VerticalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 600
            };
            return htmlTextString;
          
                         
                          
                      
           
        }

    }
}
