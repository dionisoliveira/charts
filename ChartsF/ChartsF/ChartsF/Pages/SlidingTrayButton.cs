﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{
    public class SlidingTrayButton : Button
    {
        public SlidingTrayButton(BaseContentPage page)
        {
            Text = page.NavigationPageName;
            Command = new Command(() =>
            {
                App.MasterDetailPage.Detail = new NavigationPage(page);
                App.MasterDetailPage.IsPresented = false;
            });
        }
    }
}
