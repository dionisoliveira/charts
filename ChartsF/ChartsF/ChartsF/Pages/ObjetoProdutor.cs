﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{
    public class ObjetoProdutor
    {




		public ObjetoProdutor(string produtoDescricao,string produto, string variacao, string valor,string erd,int id ,string unidade=null)
        {
            this.ProdutoDescricao = produtoDescricao;
            this.Produto = produto;
            this.Variacao = variacao;
            this.Valor = valor;
            this.Erd = erd;
            this.Unidade = unidade;
			this.id = id;
          

        }

		public int id { private set; get; }

        public string Produto { private set; get; }

        public string ProdutoDescricao { private set; get; }

        public string Variacao { private set; get; }

        public string Valor { private set; get; }

        public string Erd { private set; get; }

        public string Unidade { private set; get; }







    }
}
