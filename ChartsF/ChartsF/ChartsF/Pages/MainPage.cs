﻿using ChartsF.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ChartsF.WS.ServicesPrecosDiariosInternacionais;
using ChartsF.WS.ServicesPrecosDiariosRecebidos;
using ChartsF.WS.ServicePrecosDiariosAtacado;
using ChartsF.WS.Model;
using ChartsF.ChartBLL;
using ChartsF.ChartModel;
using System.Reflection;
using ChartsF.Pages.LayoutCell;
using System.Globalization;

namespace ChartsF.Pages
{
    public class MainPage : BaseContentPage
    {

        private string htmlPagePath = "ChartsF.WebContent.HTML.notiticas_agro.html";

        public MainPage()
        {
            var htmlSource = new HtmlWebViewSource();
            var browser = new MyWebView
            {
                BackgroundColor = Color.Red,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 600
            };
          
         
             var assembly = typeof(NoticiasAgroPage).GetTypeInfo().Assembly;

             //Get html text string
             string htmlTextString = HelpersUtils.GetStringResource(assembly, htmlPagePath);

             var htmlSources = new HtmlWebViewSource();
             var browsers = new MyWebView
             {
                 BackgroundColor = Color.Red,
                 VerticalOptions = LayoutOptions.FillAndExpand,
                 HeightRequest = 600
             };
             htmlSource.Html = htmlTextString;
             browsers.Source = htmlSource;


             var scrollView = new ScrollView()
             {
                 HorizontalOptions = LayoutOptions.Fill,
                 Orientation = ScrollOrientation.Horizontal,

                 Content = new StackLayout
                 {
                     Orientation = StackOrientation.Horizontal,
                     Children = { browser, browsers }
                 }
             };

            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children =
                          {
                          
                             scrollView
                             
                          },
                Spacing = 10,
            };
        }
    }
}
