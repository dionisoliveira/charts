﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{
    public class MenuPage : ContentPage
    {
        public ListView Menu { get; set; }

        public MenuPage()
        {

            Title = "Menu";
            Icon = "settings.png";
			BackgroundColor = ClassColor.BackgroundColor;

            Menu = new MenuListView();


          
            var layout = new StackLayout
            {
                Spacing = 0,
                VerticalOptions = LayoutOptions.FillAndExpand

            };

            layout.Children.Add(Menu);
            
      
            Content = layout;
        }

       
    }
}
