﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChartsF.Pages
{
	public class MenuListData : List<MenuItem>
	{
		public MenuListData ()
		{
			this.Add (new MenuItem () {
				Title = "Produtos",
				IconSource = "leads.png",
				TargetType = typeof(ChartsProdutosPage)
			});


			this.Add (new MenuItem () {
				Title = "Cotações",
				IconSource = "contacts.png",
				TargetType = typeof(ChartsCitiesPage)
			});
			
		}
	}
}
