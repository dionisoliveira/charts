﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{
    public enum MenuOption
    {
        Home,
        Friends,
        Profile
    }
   

    public class MenuCell : ViewCell
    {
        readonly Label _label;

        public MenuCell()
        {
            _label = new Label
            {
                VerticalOptions = LayoutOptions.Center,
				TextColor = Color.Black,
            };


            _label.SetBinding(Label.TextProperty, "Title");

            var layout = new StackLayout
            {
                Padding = new Thickness(25, 5, 10, 5),
                Spacing = 10,
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.StartAndExpand,
                Children = { _label }
            };

            View = layout;
        }
    }
}
