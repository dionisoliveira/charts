﻿

using ChartsF.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ChartsF.WS.Model;
using ChartsF.ChartBLL;
using ChartsF.ChartModel;
using System.Reflection;
using ChartsF.Pages.LayoutCell;
using System.Globalization;
using ChartsF.DTO;

namespace ChartsF.Pages
{
    public class ChartsCitiesDetailPage : BaseContentPage
    {

        #region Properties


		private string htmlPagePath = "ChartsF.WebContent.HTML.bar_line.html";
		private string htmlValue = "_______VALUE_______";
		private string htmltitulo = "_______TITULO_______";
		private string htmlproduto = "_______PRODUTO_______";
		private string htmlcss = "____CSS____";



        Button ButtonBack = null;
        private ListViewPage listViewPage;
        private StackLayout stack;
        private HtmlWebViewSource htmlSource;
        private WebView browser;
        private PrecoProdutor produtoSelecionado;
        #endregion
      
      
        ListView listViewItemCidade;
        List<Cidade> listCidade = new List<Cidade>();
     

		public ChartsCitiesDetailPage(ProdutoDTO produtoItem)
        {
			Title = "Cidades";
           //Forca sempre para mostrar o percentual
			LayoutCotacaoCidadeCell.VisibilityLabel = true;

            htmlSource = new HtmlWebViewSource();
            stack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = new Thickness(8)
            };
           
            listViewItemCidade = new ListView();
            listViewItemCidade.ItemTemplate = new DataTemplate(typeof(LayoutCotacaoCidadeCell));
			listViewItemCidade.BackgroundColor = ClassColor.BackGroudListViewColor;

            
			ObjetoProdutor cidadeSelecionada=null;
            listViewItemCidade.ItemTapped += (sender, e) =>
            {
				
                var item = (ObjetoProdutor)listViewItemCidade.SelectedItem;
				if(cidadeSelecionada == null || cidadeSelecionada.id !=item.id)
				{
					cidadeSelecionada = item;
					var precoProduto = PrecoProdutorBLL.GetPrecosByProduto_Edr(item.Produto, item.Erd).ToList().OrderByDescending(p => p.Data).ToList();
					htmlSource.Html = GenerateHTML(HelpersUtils.GetItemGraficoPrecoProdutor(precoProduto.ToList()), precoProduto.FirstOrDefault().Produto + " - " + precoProduto.FirstOrDefault().Unidade, precoProduto.FirstOrDefault().Produto);
					 browser.Source = htmlSource;
                  
        
				}
				else
				{
					

						LayoutCotacaoCidadeCell.VisibilityLabel = !LayoutCotacaoCidadeCell.VisibilityLabel;
						listViewItemCidade.ItemTemplate = new DataTemplate(typeof(LayoutCotacaoCidadeCell));

					
				}
            };
         


            

		  CriaLayoutDeCidades (produtoItem);




			browser = new WebView
			{
				
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 500
			};

			htmlSource.Html = NoticiasAgro.GetHtml();

			browser.Source = htmlSource;

			Content = null;
			Content = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children =
				{

					browser,
					listViewItemCidade,


				},
				Spacing = 10,
			};


        
		}
		private async Task CriaLayoutDeCidades(ProdutoDTO produtoItem)
        {
         
          
          
            var objectProd = new List<ObjetoProdutor>();
			var precoProduto = PrecoProdutorBLL.GetPrecosByProduto_Unidade(produtoItem.Produto,produtoItem.Unidade).OrderBy(p=> p.Data).ToList();


            foreach (PrecoProdutor produto in precoProduto)
            {
                if (!objectProd.Where(p => p.Erd == produto.Edr).Any())
                {
					var cidade = precoProduto.Where(p => p.Edr == produto.Edr && p.Valor > 0).ToList().OrderBy(p=> p.Data).Distinct().ToList();
                    var ultimoPreco = cidade.Where(p => p.Edr == produto.Edr).LastOrDefault();
					var penultimoPreco = cidade.Where(p => p.Edr == produto.Edr && p.Data != ultimoPreco.Data).LastOrDefault();
                    string variacaoValor = 0.ToString();
                    if (ultimoPreco != null)
                    {
                        if (ultimoPreco.Valor == 0)
                        {
                            variacaoValor = String.Format("{0:0.##}", 0.ToString() + " %");
                        }
                        else
                        {
							var variacao =(((ultimoPreco.Valor - penultimoPreco.Valor) / ultimoPreco.Valor) * 100);
							if(ultimoPreco.Valor > penultimoPreco.Valor)
								variacaoValor = "+"+	String.Format("{0:0.##}",  + variacao )+" %";
							else if(ultimoPreco.Valor < penultimoPreco.Valor )
								variacaoValor =	String.Format("{0:0.##}",  + variacao )+" %";
							else
								variacaoValor =	String.Format("{0:0.##}",  + variacao )+" %";

						}
                    }
					var obj = new ObjetoProdutor(string.Empty, ultimoPreco.Produto, variacaoValor, ("R$ " + String.Format("{0:0.##}", ultimoPreco.Valor.ToString())).ToString(), ultimoPreco.Edr,ultimoPreco.id,ultimoPreco.Unidade);
                    objectProd.Add(obj);
                }
            }
            listViewItemCidade.ItemsSource = objectProd;
         
        }

    


        /// <summary>
        /// 
        /// </summary>
        private void CarregaCidades()
        {
            var precoProduto = FactoryDataAccess.CreateAccess<PrecoProdutor>().SelectAll();


            foreach (PrecoProdutor preco in precoProduto)
            {

                if (!listCidade.Where(p => p.Nome == preco.Edr).Any())
                {

                    var cidade = new Cidade();
                    cidade.Nome = preco.Edr;
                    listCidade.Add(cidade);
                }


            }

            FactoryDataAccess.CreateAccess<Cidade>().InsertAll(listCidade);
            listViewItemCidade.ItemsSource = listCidade;
        }







     

		public string GenerateHTML(List<string> valueUpdate, string InformacoesDoProduto, string produtoNome = null)
		{
			try
			{
			var assembly = typeof(BarChartEntity).GetTypeInfo().Assembly;

			//Get html text string
			string htmlTextString = HelpersUtils.GetStringResource(assembly, htmlPagePath);

			//Replace the javascript method into the html
			string jsTextString = string.Empty;

			jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.highstock.js");
			jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.modules.exporting.js");
			jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.owl.carousel.min.js");



			string value = HelpersUtils.GetStringResource(assembly, htmlPagePath);
			string values = "[" + string.Join(",", valueUpdate) + "]";
			value = value.Replace(htmltitulo, InformacoesDoProduto);
			value = value.Replace(htmlproduto, produtoNome);
			value = value.Replace(htmlValue, values);
			value = value.Replace("___METHODSJS___", jsTextString);
			jsTextString = string.Empty;
			jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.owl.carousel.css");
			jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.owl.theme.css");

			jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.bootstrap.min.css");

			value = value.Replace(htmlcss, jsTextString);
			return value;
			}
			catch (Exception ex)  {
				return null;
			}


		}


    }
}

