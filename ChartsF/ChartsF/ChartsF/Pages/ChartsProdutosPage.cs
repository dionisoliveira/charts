﻿

using ChartsF.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ChartsF.WS.Model;
using ChartsF.ChartBLL;
using ChartsF.ChartModel;
using System.Reflection;
using ChartsF.Pages.LayoutCell;
using System.Globalization;
using ChartsF.DTO;
using iea.sp.gov.br;
using Connectivity.Plugin;

namespace ChartsF.Pages
{
	public class ChartsProdutosPage : BaseContentPage
	{

		#region Properties

		CustomClient custom = new CustomClient ();
		ActivityIndicator indicator;
	
		private string htmlPagePath = "ChartsF.WebContent.HTML.bar_line.html";
		private string htmlValue = "_______VALUE_______";
		private string htmltitulo = "_______TITULO_______";
		private string htmlproduto = "_______PRODUTO_______";

  
      
		private StackLayout stack;

      
      
		#endregion

		ListView listViewItemProdutos;

		public ChartsProdutosPage ()
		{
			this.BackgroundColor = ClassColor.MasterPageColor;

			stack = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Padding = new Thickness (8)
			};
			listViewItemProdutos = new ListView ();
          

			//Define item selecionado
			listViewItemProdutos.ItemTapped += (sender, e) => {

				var item = (ProdutoDTO)listViewItemProdutos.SelectedItem;
				Navigation.PushAsync (new ChartsCitiesDetailPage (item));
            
			};

			listViewItemProdutos.IsVisible = false;
			listViewItemProdutos.ItemTemplate = new DataTemplate (typeof(LayoutProdutoCell));
			listViewItemProdutos.BackgroundColor = ClassColor.BackGroudListViewColor;
			listViewItemProdutos.SeparatorColor = ClassColor.SeparatorColor;

		

			indicator = new ActivityIndicator {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.FillAndExpand,
			};


			indicator.IsRunning = true;
			Content = null;
			Content = new StackLayout {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					listViewItemProdutos,
				
					indicator
							
				},
				Spacing = 10,
			};


			Initialize ();
		
			GetPrecoDia ();
		

	

		}

		public void GetPrecoDia()
		{
			// Repeats sync process every 35s
			Device.StartTimer (new TimeSpan (0, 1, 5),()=>
				{
					if (CrossConnectivity.Current.IsConnected )
					{
						var daysDif = DateTime.Now - FactoryDataAccess.CreateAccess<Config> ().SelectAll ().FirstOrDefault ().DateLastUpdate;

					if (daysDif.Days > 0) {
						for (int i = 0; i <= daysDif.Days; i++) {
							GetPrecos (DateTime.Now.AddDays (-i));

						}
						
					}

					}
					return true;
				});

		}


		/// <summary>
		/// Gets the precos.
		/// </summary>
		/// <returns>The precos.</returns>
		/// <param name="date">Date.</param>
		public async Task GetPrecos (DateTime date)
		{
			if (CrossConnectivity.Current.IsConnected) {
				var listItem = await custom.GetPrecosProdutor (date);
				var precoProduto = ((NaDataResponse)(listItem)).Body.NaDataResult.ToList ();
				if (precoProduto.Count > 0) {
					List<PrecoProdutor> listPreco = new List<PrecoProdutor> ();
					foreach (Preco produto in precoProduto.ToList()) {
						PrecoProdutor preco = new PrecoProdutor ();
						preco.Data = produto.Data.Date;
						preco.Edr = produto.Edr;

						preco.Obs = produto.Obs;
						preco.Produto = produto.Produto;
						preco.Unidade = produto.Unidade;
						preco.Valor = produto.Valor;
						listPreco.Add (preco);

					}

					var DeleteDate = "DELETE FROM PrecoProdutor WHERE Data >= ?";
						
					object[] array = new[] { DateTime.Now.Date.ToString() };
					FactoryDataAccess.CreateAccess<PrecoProdutor> ().ExecuteAsync (DeleteDate, array);
					 FactoryDataAccess.CreateAccess<PrecoProdutor> ().InsertAll (listPreco);

					if (!FactoryDataAccess.CreateAccess<Config> ().SelectAll ().Any ()) {
						var config = new Config ();
						config.DateLastUpdate = DateTime.Now;
						FactoryDataAccess.CreateAccess<Config> ().Insert (config);
					} else {
						var config = FactoryDataAccess.CreateAccess<Config> ().SelectAll ().FirstOrDefault ();
						config.DateLastUpdate = DateTime.Now;

						FactoryDataAccess.CreateAccess<Config> ().Update (config);
					}
				}

			
			}
		}

		public async void Initialize ()
		{
			

				if (!FactoryDataAccess.CreateAccess<PrecoProdutor> ().SelectAll ().Any ()) {

					for (int i = 0; i <= 10; i++)
						await GetPrecos (DateTime.Now.AddDays (-(i * 15)));

					for (int i = 0; i <= 25; i++)
						await GetPrecos (DateTime.Now.AddDays (-i));

					var config = new Config ();
					config.DateLastUpdate = DateTime.Now;
					FactoryDataAccess.CreateAccess<Config> ().Insert (config);
			
					Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
						indicator.IsVisible = false;
						indicator.IsRunning = false;
						indicator.Color = Color.Transparent;
						
					});
					listViewItemProdutos.ItemsSource = PrecoProdutorBLL.GetProdutos ();
					listViewItemProdutos.IsVisible = true;
				} else {
					var daysDif = DateTime.Now - FactoryDataAccess.CreateAccess<Config> ().SelectAll ().FirstOrDefault ().DateLastUpdate;
					if (daysDif.Days > 0) {
						for (int i = 0; i <= daysDif.Days; i++) {
							await GetPrecos (DateTime.Now.AddDays (-i));

						}
					}


					Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
						indicator.IsVisible = false;
						indicator.IsRunning = false;
						indicator.Color = Color.Transparent;
						
					});
					listViewItemProdutos.ItemsSource = PrecoProdutorBLL.GetProdutos ();
					listViewItemProdutos.IsVisible = true;

				}
				listViewItemProdutos.IsVisible = true;

		}

	

		public string GenerateHTML (List<string> valueUpdate, string InformacoesDoProduto, string produtoNome = null)
		{
			var assembly = typeof(BarChartEntity).GetTypeInfo ().Assembly;

			//Get html text string
			string htmlTextString = HelpersUtils.GetStringResource (assembly, htmlPagePath);

			//Replace the javascript method into the html
			string jsTextString = string.Empty;

			jsTextString += HelpersUtils.GetStringResource (assembly, "ChartsF.WebContent.HTML.js.highstock.js");
			jsTextString += HelpersUtils.GetStringResource (assembly, "ChartsF.WebContent.HTML.js.modules.exporting.js");


			string value = HelpersUtils.GetStringResource (assembly, htmlPagePath);
			string values = "[" + string.Join (",", valueUpdate) + "]";
			value = value.Replace (htmltitulo, InformacoesDoProduto);
			value = value.Replace (htmlproduto, produtoNome);
			value = value.Replace (htmlValue, values);
			value = value.Replace ("___METHODSJS___", jsTextString);


			return value;

		}


	}
}

