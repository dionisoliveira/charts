﻿using System;
using Xamarin.Forms;
using ChartsF.Pages.LayoutCell;
using ChartsF.Pages;
using ChartsF.ChartBLL;
using ChartsF.WS.Model;
using System.Collections.Generic;
using System.Linq;
using ChartsF.WS;
using System.Threading.Tasks;
using iea.sp.gov.br;
using Connectivity.Plugin;
using System.Threading;



namespace ChartsF
{
	public class ChartsCitiesPage :ContentPage
	{
		ListView listViewItemCidade;
		CustomClient custom = new CustomClient ();
		ActivityIndicator indicator;

		public  ChartsCitiesPage ()
		{
			indicator = new ActivityIndicator {
				VerticalOptions = LayoutOptions.CenterAndExpand,
				HorizontalOptions = LayoutOptions.CenterAndExpand,

			};




			this.BackgroundColor = ClassColor.MasterPageColor;
			indicator.IsRunning = true;
			listViewItemCidade = new ListView ();
			listViewItemCidade.HasUnevenRows = true;
			listViewItemCidade.SeparatorColor = ClassColor.SeparatorColor;
			listViewItemCidade.BackgroundColor = ClassColor.BackGroudListViewColor;

			Content = null;
			Content = new StackLayout {
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Children = {
					listViewItemCidade,
					indicator
				},
				Spacing = 10,
			};
			Title = "Cidades";


			listViewItemCidade.ItemTapped += async (sender, e) => {
				var item = (Cidade)listViewItemCidade.SelectedItem;
				await Navigation.PushAsync (new ChartsProdutoDetailPage (item)
				);
			};
			listViewItemCidade.IsVisible = false;
			listViewItemCidade.ItemTemplate = new DataTemplate (typeof(LayoutCidadeCell));
	

			Initialize ();

		}
		public async void Initialize ()
		{
			

				if (!FactoryDataAccess.CreateAccess<PrecoProdutor> ().SelectAll ().Any ()) 
				{
				
					for (int i = 0; i <= 10; i++)
						await GetPrecos (DateTime.Now.AddDays (-(i * 15)));

					for (int i = 0; i <= 25; i++)
						await GetPrecos (DateTime.Now.AddDays (-i));

					var config = new Config ();
					config.DateLastUpdate = DateTime.Now;
					FactoryDataAccess.CreateAccess<Config> ().Insert (config);
					if (!FactoryDataAccess.CreateAccess<Cidade> ().SelectAll ().Any ())
						ProcessCities ();
					Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
						indicator.IsVisible = false;
						indicator.IsRunning = false;
						indicator.Color = Color.Transparent;
						listViewItemCidade.IsVisible = true;
						listViewItemCidade.ItemsSource = FactoryDataAccess.CreateAccess<Cidade> ().SelectAll ().OrderBy (p => p.Nome);

					});
				} else {
					var daysDif = DateTime.Now - FactoryDataAccess.CreateAccess<Config> ().SelectAll ().FirstOrDefault ().DateLastUpdate;

					if (daysDif.Days > 0) {
						for (int i = 0; i <= daysDif.Days; i++) {
							await GetPrecos (DateTime.Now.AddDays (-i));
						}
				
					}
					var config = FactoryDataAccess.CreateAccess<Config> ().SelectAll ().FirstOrDefault ();
					config.DateLastUpdate = DateTime.Now;

					FactoryDataAccess.CreateAccess<Config> ().Update (config);


					if (!FactoryDataAccess.CreateAccess<Cidade> ().SelectAll ().Any ())
						ProcessCities ();
					Xamarin.Forms.Device.BeginInvokeOnMainThread (() => {
						indicator.IsVisible = false;
						indicator.IsRunning = false;
						indicator.Color = Color.Transparent;
					
						listViewItemCidade.IsVisible = true;
						listViewItemCidade.ItemsSource = FactoryDataAccess.CreateAccess<Cidade> ().SelectAll ().OrderBy (p => p.Nome);
					});
				}

		}

		public void ProcessCities ()
		{

			var precoProduto = FactoryDataAccess.CreateAccess<PrecoProdutor> ().SelectAll ();

			var listCidade = new List<Cidade> ();
			foreach (PrecoProdutor preco in precoProduto) {

				if (!listCidade.Where (p => p.Nome == preco.Edr).Any ()) {

					var cidade = new Cidade ();
					cidade.Nome = preco.Edr;
					listCidade.Add (cidade);
				}


			}

			FactoryDataAccess.CreateAccess<Cidade> ().InsertAll (listCidade);

		}

		/// <summary>
		/// Gets the precos.
		/// </summary>
		/// <returns>The precos.</returns>
		/// <param name="date">Date.</param>
		public async Task GetPrecos (DateTime date)
		{
			if (CrossConnectivity.Current.IsConnected) {
				var listItem = await custom.GetPrecosProdutor (date);
				var precoProduto = ((NaDataResponse)(listItem)).Body.NaDataResult.ToList ();

				if (precoProduto.Count > 0) {
					List<PrecoProdutor> listPreco = new List<PrecoProdutor> ();
					foreach (Preco produto in precoProduto.ToList()) {
						PrecoProdutor preco = new PrecoProdutor ();
						preco.Data = produto.Data;
						preco.Edr = produto.Edr;

						preco.Obs = produto.Obs;
						preco.Produto = produto.Produto;
						preco.Unidade = produto.Unidade;
						preco.Valor = produto.Valor;
						listPreco.Add (preco);

					}
					FactoryDataAccess.CreateAccess<PrecoProdutor> ().InsertAll (listPreco);

					var config = FactoryDataAccess.CreateAccess<Config> ().SelectAll ().FirstOrDefault ();
					config.DateLastUpdate = DateTime.Now;

					FactoryDataAccess.CreateAccess<Config> ().Update (config);
				}
			}
		}
	}
}

