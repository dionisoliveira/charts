﻿using ChartsF.Pages;
using System;
using Xamarin.Forms;

namespace ChartsF
{
    public class RootPage : Xamarin.Forms.MasterDetailPage
    {

      
        public RootPage()
        {
            var menuPage = new MenuPage();

            menuPage.Menu.ItemSelected += (sender, e) => NavigateTo(e.SelectedItem as ChartsF.Pages.MenuItem);

            Master = menuPage;
			Detail = new CustomNavigationPage(new ChartsProdutosPage())
            {
				BarBackgroundColor = ClassColor.BackgroundColor,
				BarTextColor = Color.White,
            };
           

          
        }

        void NavigateTo(ChartsF.Pages.MenuItem menu)
        {
            Page displayPage = (Page)Activator.CreateInstance(menu.TargetType);

            Detail = new CustomNavigationPage(displayPage)
            {
				BarBackgroundColor = ClassColor.BackgroundColor,
				BarTextColor = Color.White,
            };
            IsPresented = false;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
          
        }
    }
}
      