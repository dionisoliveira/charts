﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{
    public class PageMain : BaseContentPage
    {

        CarouselPageMain carousel;

        public PageMain()
        {
            carousel = new CarouselPageMain();

            AbsoluteLayout simpleLayout = new AbsoluteLayout
            {
                BackgroundColor = Color.Blue.WithLuminosity(0.9),
                VerticalOptions = LayoutOptions.FillAndExpand
            };
          

            this.Content = new StackLayout
            {
                Children = {
                 
                    simpleLayout
                }
            };
        }
    }
}
