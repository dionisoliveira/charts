﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text;
using ChartsF.WS;


namespace ChartsF.Pages.Stock
{
    public class GetHistoriclStock
    {

        public List<HistoricalStock> GetHistorical(string stock,string year)
        {
            List<HistoricalStock> retval = new List<HistoricalStock>();


            CustomClient clientweb = new CustomClient();

            string value = clientweb.DownloadStrings(string.Format("http://ichart.finance.yahoo.com/table.csv?s={0}&c={1}", stock, year));






            var data = value;

            string[] rows = data.Split('\n');

            //First row is headers so Ignore it
            for (int i = 1; i < rows.Length; i++)
            {
                if (rows[i].Replace("n", "").Trim() == "") continue;

                string[] cols = rows[i].Split(',');

                HistoricalStock hs = new HistoricalStock();
                hs.Date = Convert.ToDateTime(cols[0]);
                hs.Open =cols[1];
                hs.High = cols[2];
                hs.Low =cols[3];
                hs.Close =cols[4];
                hs.Volume = cols[5];
                hs.AdjClose =cols[6];

                retval.Add(hs);
            }

            return retval;
        }
    }
}
