﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SIGGA.GESTAOPARTICIPATIVA.CustomUIComponents.View
{
    public class CustomWebView : WebView
    {
        #region Properties

        public Action GoBack;
        public Action CanGoBackUpdater;

        public bool CanGoBack;
        public bool ShowLoading = true;

        #endregion
        
    }
}
