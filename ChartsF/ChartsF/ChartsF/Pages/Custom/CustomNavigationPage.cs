﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ChartsF.Pages
{
    public class CustomNavigationPage: NavigationPage
    {

        public bool Animated { get; private set; }
        public CustomNavigationPage()
        {

        }
        public CustomNavigationPage(Page root)
            : base(root)
        {
          

        }

        
        public async Task PushAsync(Page page, bool animated = true)
        {
            Animated = animated;
            await base.PushAsync(page);
            await Task.Run(delegate
            {
               
            });
        }

        public async Task<Page> PopAsync(bool animated = true)
        {
            Animated = animated;
            var task = await base.PopAsync();
            await Task.Run(delegate
            {
               
            });
            return task;
        }

    }
}
