﻿

using ChartsF.WS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ChartsF.WS.Model;
using ChartsF.ChartBLL;
using ChartsF.ChartModel;
using System.Reflection;
using ChartsF.Pages.LayoutCell;
using System.Globalization;

namespace ChartsF.Pages
{
    public class ChartsProdutoDetailPage : BaseContentPage
    {

        #region Properties


        private string htmlPagePath = "ChartsF.WebContent.HTML.bar_line.html";
        private string htmlValue = "_______VALUE_______";
        private string htmltitulo = "_______TITULO_______";
        private string htmlproduto = "_______PRODUTO_______";
        private string htmlcss = "____CSS____";

	
        private StackLayout stack;
        private HtmlWebViewSource htmlSource;
		private WebView browser;

        #endregion

        ListView listViewItemProdutos;

		public ChartsProdutoDetailPage(Cidade cidade)
        {
			Title = "Produtos";
			//Forca sempre para mostrar o percentual
			LayoutCotacaoCidadeCell.VisibilityLabel = true;

			listViewItemProdutos = new ListView();
			listViewItemProdutos.HasUnevenRows = true;
			listViewItemProdutos.SeparatorColor = ClassColor.SeparatorColor;
			listViewItemProdutos.ItemTemplate = new DataTemplate(typeof(LayoutCotacaoCell));

            htmlSource = new HtmlWebViewSource();
            stack = new StackLayout
            {
                Orientation = StackOrientation.Vertical,
                Padding = new Thickness(8)
            };

            browser = new WebView
            {
                BackgroundColor = Color.Red,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HeightRequest = 500
            };
            htmlSource.Html = NoticiasAgro.GetHtml();
		
            browser.Source = htmlSource;

			if (Device.OS == TargetPlatform.iOS) {
				htmlSource.BaseUrl = NoticiasAgro.GetHtml();
			} 
          
			ObjetoProdutor produtoSelecionado=null;
            //Define item selecionado
            listViewItemProdutos.ItemTapped += (sender, e) =>
            {
				var item = (ObjetoProdutor)listViewItemProdutos.SelectedItem;
				if(produtoSelecionado == null || produtoSelecionado.id !=item.id)
				{
              
					var precoProduto = PrecoProdutorBLL.GetPrecosByProduto_Edr(item.Produto, cidade.Nome).ToList().OrderByDescending(p => p.Data).ToList();
                	htmlSource.Html = GenerateHTML(HelpersUtils.GetItemGraficoPrecoProdutor(precoProduto.ToList()), precoProduto.FirstOrDefault().Produto + " - " + precoProduto.FirstOrDefault().Unidade, precoProduto.FirstOrDefault().Produto);
                	browser.Source = htmlSource;
					produtoSelecionado = item;
				}
				else
				{
					LayoutCotacaoCell.VisibilityLabel = !LayoutCotacaoCell.VisibilityLabel;
					listViewItemProdutos.ItemTemplate = new DataTemplate(typeof(LayoutCotacaoCell));
				}                
            };
				 CriaLayoutDeProdutos (cidade);

        }

		private async Task CriaLayoutDeProdutos(Cidade item)
        {

            listViewItemProdutos.IsVisible = true;
         
            var objectProd = new List<ObjetoProdutor>();
            var precoProduto = PrecoProdutorBLL.GetPrecosByEdr(item.Nome).ToList();
            decimal? valorProduto = 0;

            foreach (PrecoProdutor produto in precoProduto)
            {
                if (!objectProd.Where(p => p.ProdutoDescricao == produto.Produto + " (" + produto.Unidade.ToUpper() + ")").Any())
                {
					var variacaoProduto = new PrecoProdutor ();
					var days = 0;
					do
					{	days+=1;

						variacaoProduto = PrecoProdutorBLL.GetPrecosByEdr_Produto_Data (item.Nome, produto.Produto, produto.Data.Date.AddDays (-days).Date).FirstOrDefault ();

						if(variacaoProduto!= null && (variacaoProduto.Valor == 0 || variacaoProduto.Valor == null))
							variacaoProduto= null;
					}
					while(variacaoProduto == null && days < 12);
                   
                    string variacaoValor = 0.ToString();
                    if (variacaoProduto != null)
                    {
                        if (string.IsNullOrEmpty(produto.Valor.ToString()))
                        {
                            variacaoValor = String.Format("{0:0.##}", 0.ToString()) + " %";
							var valorPrecoProduto = precoProduto.Where(p => p.Valor > 0 && p.Edr == produto.Edr && p.Produto == produto.Produto).FirstOrDefault();
                            if (valorPrecoProduto != null)
                            {
                                valorProduto = valorPrecoProduto.Valor;
                            }
                            else
                            {
                                valorProduto = 0;
                            }
                        }
                        else
                        {
							decimal valorVariacao = 0;
							valorVariacao = Convert.ToDecimal(((produto.Valor - variacaoProduto.Valor) / produto.Valor) * 100);
							if(produto.Valor > variacaoProduto.Valor)
								variacaoValor = "+"+	String.Format("{0:0.##}",  + valorVariacao )+" %";
							else if(produto.Valor < variacaoProduto.Valor )
								variacaoValor = String.Format("{0:0.##}",  + valorVariacao )+" %";
							else
								variacaoValor =	String.Format("{0:0.##}",  + valorVariacao )+" %";
							
					          valorProduto = produto.Valor;
                        }
                    }
                    else
                    {
                        variacaoValor = String.Format("{0:0.##}", 0.ToString()) + " %";
                        if (string.IsNullOrEmpty(produto.Valor.ToString()))
                        {
							var valorPrecoProduto = precoProduto.Where(p => p.Valor > 0 && p.Edr == produto.Edr && p.Produto == produto.Produto).FirstOrDefault();
                            if (valorPrecoProduto != null)
                            {
                                valorProduto = valorPrecoProduto.Valor;
                            }
                            else
                            {
                                valorProduto = 0;
                            }
                        }

                    }
					var obj = new ObjetoProdutor(produto.Produto + " (" + produto.Unidade.ToUpper() + ")", 
						produto.Produto.ToString(), 
												 variacaoValor.ToString(), 
												 ("R$ " + String.Format("{0:C}", valorProduto.ToString())).ToString(), 
												
												 produto.Edr,
												 produto.id
												 );
                    objectProd.Add(obj);
                }
            }
            listViewItemProdutos.ItemsSource = objectProd;
			listViewItemProdutos.BackgroundColor = ClassColor.BackGroudListViewColor;
			await GenerateGraphics(null, null);
        }


        public async Task GenerateGraphics(List<string> valueUpdate, List<ObjetoProdutor> lists)
        {
            browser.Source = htmlSource;
			if (valueUpdate != null) {
				htmlSource.Html = GenerateHTML (valueUpdate, "");
				htmlSource.BaseUrl = GenerateHTML (valueUpdate, "");
			}

            Content = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children =
                          {

					browser,
					listViewItemProdutos,
				
                          
                          },
                Spacing = 10,
            };
			this.Content.BackgroundColor = ClassColor.BackgroundColor;

        }

        public string GenerateHTML(List<string> valueUpdate, string InformacoesDoProduto, string produtoNome = null)
        {
			try
			{
            var assembly = typeof(BarChartEntity).GetTypeInfo().Assembly;

            //Get html text string
            string htmlTextString = HelpersUtils.GetStringResource(assembly, htmlPagePath);

            //Replace the javascript method into the html
            string jsTextString = string.Empty;

            jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.highstock.js");
            jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.modules.exporting.js");
            jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.owl.carousel.min.js");



            string value = HelpersUtils.GetStringResource(assembly, htmlPagePath);
            string values = "[" + string.Join(",", valueUpdate) + "]";
            value = value.Replace(htmltitulo, InformacoesDoProduto);
            value = value.Replace(htmlproduto, produtoNome);
            value = value.Replace(htmlValue, values);
            value = value.Replace("___METHODSJS___", jsTextString);
            jsTextString = string.Empty;
            jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.owl.carousel.css");
            jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.owl.theme.css");

            jsTextString += HelpersUtils.GetStringResource(assembly, "ChartsF.WebContent.HTML.js.bootstrap.min.css");

            value = value.Replace(htmlcss, jsTextString);
            return value;
			}
			catch (Exception ex) {
				return null;
			}

        }


    }
}

